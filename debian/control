Source: radlib
Priority: optional
Maintainer: Debian IoT Maintainers <debian-iot-maintainers@lists.alioth.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13)
	, libsqlite3-dev
Standards-Version: 4.6.2
Section: libs
Homepage: http://www.radlib.teel.ws
Vcs-Browser: https://salsa.debian.org/debian-iot-team/radlib
Vcs-Git: https://salsa.debian.org/debian-iot-team/radlib.git
Rules-Requires-Root: no

Package: radlib-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: librad0 (= ${binary:Version}), ${misc:Depends}, libsqlite3-dev
Description: development file for librad0
 radlib is a C language library developed to abstract details of interprocess
 communications and common linux/unix system facilities so that application
 developers can concentrate on application solutions. It encourages developers
 (whether expert or novice) to use a proven paradigm of event-driven,
 asynchronous design. By abstracting interprocess messaging, events, timers,
 and any I/O device that can be represented as a file descriptor, radlib
 simplifies the implementation of multi-purpose processes, as well as multi-
 process applications.
 .
 This is the package needed for development.

Package: librad0-tools
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libsqlite3-0, librad0 (= ${binary:Version})
Description: tools for rapid application development library
 radlib is a C language library developed to abstract details of interprocess
 communications and common linux/unix system facilities so that application
 developers can concentrate on application solutions. It encourages developers
 (whether expert or novice) to use a proven paradigm of event-driven,
 asynchronous design. By abstracting interprocess messaging, events, timers,
 and any I/O device that can be represented as a file descriptor, radlib
 simplifies the implementation of multi-purpose processes, as well as multi-
 process applications.
 .
 This is a package containing some tools.

Package: librad0
Architecture: any
Multi-Arch: same
Suggests: librad0-tools (= ${binary:Version})
Depends: ${shlibs:Depends}, ${misc:Depends}, libsqlite3-0
Description: rapid application development library
 radlib is a C language library developed to abstract details of interprocess
 communications and common linux/unix system facilities so that application
 developers can concentrate on application solutions. It encourages developers
 (whether expert or novice) to use a proven paradigm of event-driven,
 asynchronous design. By abstracting interprocess messaging, events, timers,
 and any I/O device that can be represented as a file descriptor, radlib
 simplifies the implementation of multi-purpose processes, as well as multi-
 process applications.
